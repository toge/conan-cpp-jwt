import os

from conans import ConanFile, tools

class CppJwtConan(ConanFile):
    name = "cpp-jwt"
    version = "1.3"
    license = "MIT"
    author = "toge.mail@gmail.com"
    homepage = "https://github.com/arun11299/cpp-jwt"
    url = "https://bitbucket.org/toge/conan-cpp-jwt/"
    description = "JSON Web Token library for C++"
    topics = ("jwt", "jwt-header", "security")
    no_copy_source = True
    requires = ("nlohmann_json/[>= 3.7.0]", "openssl/[>= 1.1.1d]")
    # No settings/options are necessary, this is header only

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get("https://github.com/arun11299/cpp-jwt/archive/v{}.zip".format(self.version))
        os.rename("cpp-jwt-{}".format(self.version), "cpp-jwt")

    def package(self):
        self.copy("*.hpp", "include/jwt", src="cpp-jwt/include/jwt")
        self.copy("*.ipp", "include/jwt", src="cpp-jwt/include/jwt")
