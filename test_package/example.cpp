#include <iostream>
#include "jwt/jwt.hpp"

int main() {
    auto key = "secret";
    auto obj = jwt::jwt_object { jwt::params::algorithm("HS256"), jwt::params::payload({{"some", "payload"}}), jwt::params::secret(key) };

    std::cout << obj.signature() << std::endl;

    return 0;
}
